/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupocice.finanzas;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.text.SimpleDateFormat; 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author gmendez
 */
public class OracleDB {
    
    private Connection conn;    
    SimpleDateFormat formatterMDY;
    SimpleDateFormat formatterYMD;
    
    public OracleDB(){
        this.conn = null;        
        
        formatterMDY = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"); 
        formatterYMD = new SimpleDateFormat("yyyy-MM-dd"); 
    }
    
    public void close() throws SQLException {
        if (this.conn!=null){
            this.conn.close();
            this.conn = null;
        }
    }
    
    public ResultSet doQuery(String sqlStmt){
        
        ResultSet result = null;          
        Statement stmt = null;
                                
        try {
            
            if (this.conn == null){            
                return null;
            }
                        
            stmt = this.conn.createStatement();
            result = stmt.executeQuery(sqlStmt);            
                                                           
                                
        } catch(SQLException sqle){
            System.out.println("OracleDB: Error en consulta: "+sqle.getMessage());
            result = null;
        }
        return result;                
    }

    public boolean execute(String sqlStmt){
        boolean result = true;
        
        Statement stmt = null;
                                
        try {
            
            if (this.conn == null){            
                return false;
            }
                        
            stmt = this.conn.createStatement();
            result = stmt.execute(sqlStmt);
            
            this.conn.commit();                                                           
                                
        } catch(SQLException sqle){
            System.out.println("OracleDB: Error en ejecucion: "+sqle.getMessage());
            result = false;
        }
        return result;                
    }
    
    
    public void connect()  throws SQLException {
        
        if (this.conn==null){

            try {
                //Driver JDBC
                Class.forName("oracle.jdbc.driver.OracleDriver");
                
                String connURL = "jdbc:oracle:thin:@10.70.10.174:1521/EBSCICE"; 
                
                //El root es el nombre de usuario por default. No hay contraseña
                String usuario = "apps";
                String pass = "apps";
                //Se inicia la conexión
                this.conn = DriverManager.getConnection(connURL, usuario, pass);

            } catch (ClassNotFoundException ex) {
                System.out.println("Error en la conexión a la base de datos: " + ex.getMessage());
                this.conn = null;
                                
                throw new SQLException();
                
            } catch (SQLException ex) {
                System.out.println("Error en la conexión a la base de datos: " + ex.getMessage());
                this.conn = null;
                throw new SQLException();
                
            } catch (Exception ex) {
                System.out.println("Error en la conexión a la base de datos: " + ex.getMessage());
                this.conn = null;
                throw new SQLException();
            }        
        }
        
        return; // this.conn;
    }    
    
    public void connectTest()  throws SQLException {
        
        if (this.conn==null){

            try {
                //Driver JDBC
                Class.forName("oracle.jdbc.driver.OracleDriver");                
                
                String connURL = "jdbc:oracle:thin:@10.70.10.89:1521/EBSCLONE";
                
                //El root es el nombre de usuario por default. No hay contraseña
                String usuario = "apps";
                String pass = "apps";
                //Se inicia la conexión
                this.conn = DriverManager.getConnection(connURL, usuario, pass);

            } catch (ClassNotFoundException ex) {
                System.out.println("Error en la conexión a la base de datos: " + ex.getMessage());
                this.conn = null;
                                
                throw new SQLException();
                
            } catch (SQLException ex) {
                System.out.println("Error en la conexión a la base de datos: " + ex.getMessage());
                this.conn = null;
                throw new SQLException();
                
            } catch (Exception ex) {
                System.out.println("Error en la conexión a la base de datos: " + ex.getMessage());
                this.conn = null;
                throw new SQLException();
            }        
        }
        
        return; // this.conn;
    }        
    
}
