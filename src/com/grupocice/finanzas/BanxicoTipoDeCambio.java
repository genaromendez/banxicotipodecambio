/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupocice.finanzas;

import com.grupocice.banxico.*;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import java.sql.SQLException;
import java.util.HashMap;
import org.xml.sax.SAXException;

/**
 *
 * @author macpro1
 */
public class BanxicoTipoDeCambio {
    
    HashMap<String, String> hmapCodes;
    
    public static final int FECHA = 0;
    public static final int VALOR = 1;
    
    HashMap<String, String[]> hmapTiposDeCambio;
    
    BanxicoTipoDeCambio(){
            hmapCodes = new HashMap<String, String>();            
            hmapCodes.put("SF46410","EUR");
            hmapCodes.put("SF60632","CAD"); // Dolar canadiense
            hmapCodes.put("SF43718","FIX");
            hmapCodes.put("SF46407","GBP"); // Libra Esterlina
            hmapCodes.put("SF46406","JPY"); // Yen Japonés
            hmapCodes.put("SF60653","USD");               
            
            this.hmapTiposDeCambio = new HashMap<String, String[]>();               
    };
        
    public HashMap<String, String[]> getTiposCambio(){
        hmapTiposDeCambio = new HashMap<String, String[]>();   
        
        try {                        
            DgieWS_Impl wsImpl = new DgieWS_Impl();            
            DgieWSPort wsPort = wsImpl.getDgieWSPort();
            
            String resp = wsPort.tiposDeCambioBanxico();                       
            
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            StringBuilder xmlStringBuilder = new StringBuilder();
            xmlStringBuilder.append(resp);
            
            ByteArrayInputStream input = new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
            
            Document doc = builder.parse(input);
            
            // Se obtienen todos los nodos del grupo de Series
            NodeList nSeries = doc.getElementsByTagName("bm:Series"); 
            
            for(int i=0;i<nSeries.getLength();i++){
                NodeList nObs = nSeries.item(i).getChildNodes();
                
                String idSerie = nSeries.item(i).getAttributes().getNamedItem("IDSERIE").getNodeValue();
                
                // Se obtienen todos los nodos bm:Obs de cada bm:Series
                for(int j=0;j<nObs.getLength();j++){
                    Node n = nObs.item(j);
                    if (n.getNodeName().equals("bm:Obs")){                        
                        NamedNodeMap nnm = n.getAttributes();
                        
                        String sFecha = nnm.getNamedItem("TIME_PERIOD").getNodeValue();                                                        
                        String sTC    = nnm.getNamedItem("OBS_VALUE").getNodeValue();                                                        
                        String sFechaTC[] = {sFecha,sTC};
                        
                        hmapTiposDeCambio.put(hmapCodes.get(idSerie),sFechaTC);                                                                                       
                    }
                }                                                               
            }                                    
        } catch (RemoteException ex) {
            Logger.getLogger(BanxicoTipoDeCambio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BanxicoTipoDeCambio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(BanxicoTipoDeCambio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(BanxicoTipoDeCambio.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return hmapTiposDeCambio;
        
    };
    
    // Codigo de moneda en ISO 4217 mas el código FIX
    public String[] getTipoCambio(String moneda){
        return this.hmapTiposDeCambio.get(moneda);
    }
    
    public void print(){
        for(HashMap.Entry<String, String[]> entry : hmapTiposDeCambio.entrySet()) {
            String key = entry.getKey();
            String[] value = entry.getValue();
            System.out.printf("%s %s %s\n",key,value[0],value[1]);
        }        
    }

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {

        BanxicoTipoDeCambio btc = new BanxicoTipoDeCambio();    
        
        HashMap<String, String[]> hmaptc = btc.getTiposCambio();
        String tcUSD[] = btc.getTipoCambio("USD");
        String tcEUR[] = btc.getTipoCambio("EUR");
        
        btc.print();
               
        
        OracleDB odb = new OracleDB();
        
        String sqlStmtBase = "INSERT INTO GL_DAILY_RATES_INTERFACE (FROM_CURRENCY, TO_CURRENCY, FROM_CONVERSION_DATE,TO_CONVERSION_DATE,USER_CONVERSION_TYPE,CONVERSION_RATE,MODE_FLAG,LAUNCH_RATE_CHANGE,USER_ID) "+
                                           " VALUES ('%s','MXN',TO_DATE('%s','YYYY-MM-DD'),TO_DATE('%s','YYYY-MM-DD'),'Corporate',%s,'I','Y',1203)";
        
        String sqlStmt = "";
        try {
            odb.connectTest();
            
            sqlStmt = String.format(sqlStmtBase,"USD",tcUSD[BanxicoTipoDeCambio.FECHA],tcUSD[BanxicoTipoDeCambio.FECHA],tcUSD[BanxicoTipoDeCambio.VALOR] );
            
            System.out.println(sqlStmt);
            odb.execute(sqlStmt);

            sqlStmt = String.format(sqlStmtBase,"EUR",tcEUR[BanxicoTipoDeCambio.FECHA],tcEUR[BanxicoTipoDeCambio.FECHA],tcEUR[BanxicoTipoDeCambio.VALOR] );
            
            System.out.println(sqlStmt);
            odb.execute(sqlStmt);
            
            odb.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(BanxicoTipoDeCambio.class.getName()).log(Level.SEVERE, null, ex);
        }
                                      
    }
    
}


/*
    public static String respTmp =  "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" +
                                    "<CompactData xmlns=\"http://www.SDMX.org/resources/SDMXML/schemas/v1_0/message\"\n" +
                                    "xmlns:bm=\"http://www.banxico.org.mx/structure/key_families/dgie/sie/series/compact\"\n" +
                                    "xmlns:compact=\"http://www.SDMX.org/resources/SDMXML/schemas/v1_0/compact\"\n" +
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.SDMX.org/resources/SDMXML/schemas/v1_0/message SDMXMessage.xsd\n" +
                                    "http://www.banxico.org.mx/structure/key_families/dgie/sie/series/compact BANXICO_DGIE_SIE_Compact.xsd\n" +
                                    "http://www.SDMX.org/resources/SDMXML/schemas/v1_0/compact SDMXCompactData.xsd\" >\n" +
                                    "	<Header>\n" +
                                    "		<ID>TIPOSDECAMBIO</ID>\n" +
                                    "		<Test>false</Test>\n" +
                                    "		<Truncated>false</Truncated>\n" +
                                    "		<Name xml:lang=\"sp\">Tipos de Cambio</Name>\n" +
                                    "		<Prepared>2018-08-08 15:44:56.260</Prepared>\n" +
                                    "		<Sender id=\"BANXICO\">\n" +
                                    "			<Name xml:lang=\"sp\">Banco de México</Name>\n" +
                                    "			<Contact>\n" +
                                    "			<Name xml:lang=\"sp\">Subgerencia de Desarrollo de Sistemas</Name>\n" +
                                    "			<Telephone>(01 55)52372678</Telephone>\n" +
                                    "			</Contact>\n" +
                                    "		</Sender>\n" +
                                    "		<DataSetAction>Update</DataSetAction>\n" +
                                    "		<Extracted>2018-08-08 15:44:56.260</Extracted>\n" +
                                    "	</Header>\n" +
                                    "	<bm:DataSet>\n" +
                                    "		<bm:SiblingGroup BANXICO_FREQ=\"Dia\" TIME_FORMAT=\"P1D\"/>\n" +
                                    "		<bm:Series TITULO=\"Cotización de las divisas que conforman la canasta del DEG Respecto al peso mexicano Euro\" IDSERIE=\"SF46410\" BANXICO_FREQ=\"Dia\" BANXICO_FIGURE_TYPE=\"TipoCambio\" BANXICO_UNIT_TYPE=\"Peso\">\n" +
                                    "			<bm:Obs TIME_PERIOD=\"2018-08-08\" OBS_VALUE=\"21.398568\"/>\n" +
                                    "		</bm:Series>\n" +
                                    "		<bm:Series TITULO=\"Cotización de la divisa Respecto al peso mexicano Dólar Canadiense\" IDSERIE=\"SF60632\" BANXICO_FREQ=\"Dia\" BANXICO_FIGURE_TYPE=\"TipoCambio\" BANXICO_UNIT_TYPE=\"Peso\">\n" +
                                    "			<bm:Obs TIME_PERIOD=\"2018-08-08\" OBS_VALUE=\"14.1483\"/>\n" +
                                    "		</bm:Series>\n" +
                                    "		<bm:Series TITULO=\"Tipo de cambio                                          Pesos por dólar E.U.A. Tipo de cambio para solventar obligaciones denominadas en moneda extranjera Fecha de determinación (FIX)\" IDSERIE=\"SF43718\" BANXICO_FREQ=\"Dia\" BANXICO_FIGURE_TYPE=\"TipoCambio\" BANXICO_UNIT_TYPE=\"PesoxDoll\">\n" +
                                    "			<bm:Obs TIME_PERIOD=\"2018-08-08\" OBS_VALUE=\"18.4542\"/>\n" +
                                    "		</bm:Series>\n" +
                                    "		<bm:Series TITULO=\"Cotización de las divisas que conforman la canasta del DEG Respecto al peso mexicano Libra esterlina\" IDSERIE=\"SF46407\" BANXICO_FREQ=\"Dia\" BANXICO_FIGURE_TYPE=\"TipoCambio\" BANXICO_UNIT_TYPE=\"Peso\">\n" +
                                    "			<bm:Obs TIME_PERIOD=\"2018-08-08\" OBS_VALUE=\"23.733947\"/>\n" +
                                    "		</bm:Series>\n" +
                                    "		<bm:Series TITULO=\"Cotización de las divisas que conforman la canasta del DEG Respecto al peso mexicano Yen japonés\" IDSERIE=\"SF46406\" BANXICO_FREQ=\"Dia\" BANXICO_FIGURE_TYPE=\"TipoCambio\" BANXICO_UNIT_TYPE=\"Peso\">\n" +
                                    "			<bm:Obs TIME_PERIOD=\"2018-08-08\" OBS_VALUE=\"0.166187\"/>\n" +
                                    "		</bm:Series>\n" +
                                    "		<bm:Series TITULO=\"Tipo de cambio pesos por dólar E.U.A. Tipo de cambio para solventar obligaciones denominadas en moneda extranjera Fecha de liquidación\" IDSERIE=\"SF60653\" BANXICO_FREQ=\"Dia\" BANXICO_FIGURE_TYPE=\"TipoCambio\" BANXICO_UNIT_TYPE=\"PesoxDoll\">\n" +
                                    "			<bm:Obs TIME_PERIOD=\"2018-08-08\" OBS_VALUE=\"18.5433\"/>\n" +
                                    "		</bm:Series>\n" +
                                    "	</bm:DataSet>\n" +
                                    "</CompactData>";
*/